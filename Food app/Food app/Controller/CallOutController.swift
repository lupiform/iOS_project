import UIKit
import MapKit
import FirebaseDatabase
import FirebaseStorage

class CallOutController: UIViewController, MKMapViewDelegate{
    
    var annotation : MyAnnotation?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionField: UITextView!
    @IBOutlet weak var timeField: UILabel!
    
    @IBOutlet weak var image: UIImageView!

    override func viewDidLoad() {
        super .viewDidLoad()
        
        titleLabel.text = annotation?.title
        descriptionField.text = annotation?.desc
        timeField.text = annotation?.time
        
        
        if let url = annotation?.imageURL, url.isEmpty == false, url != "empty" {
            let storageRef = Storage.storage().reference(forURL: url)
            storageRef.downloadURL { (url, error) in
                let data = NSData(contentsOf: url!)
                let image = UIImage(data: data! as Data)
                self.image.image = image
            }
        } else {
            	
            self.image.image = UIImage(named: "hihi")
        }
    }
    
}
