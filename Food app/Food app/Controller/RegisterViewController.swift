//  Copyright © 2017 cambri. All rights reserved.

import UIKit
import Firebase

class RegisterViewController: UIViewController {
    
    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var passwordTextfield: UITextField!
    @IBOutlet weak var image: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideKeyboardWhenTappedAround()
        image.image = UIImage(named: "hihi")    
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func alreadyAccountPressed(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func registerPressed(_ sender: Any) {

        if emailTextfield.text == "" || passwordTextfield.text == ""
        {
            self.showAlert(title: "Info", message: "Please enter all text fields")
            
        } else {
            
            Auth.auth().createUser(withEmail: emailTextfield.text!, password: passwordTextfield.text!, completion: { user, error in
                print(error as Any)
                    
                    if error != nil {
                        
                        if let errorCode = AuthErrorCode(rawValue: error!._code) {
                        
                            switch errorCode {
                            
                            case .emailAlreadyInUse:
                                self.showAlert(title: "Info", message: "The email you are trying to use has already been registred")
                        
                            case .invalidEmail:
                                self.showAlert(title: "Info", message: "The email address is badly formatted")
                            
                            case .networkError:
                                self.showAlert(title: "Info", message: "Network issues")
                            default:
                                self.showAlert(title: "Info", message: "Unknown error")
                                }
                        }
                        else {
                            print("Everything went oki")
                            }
                        }
                    else {
                        self.performSegue(withIdentifier: "goToMap", sender: self)
                    }
                }
            )
        }
    }
}

