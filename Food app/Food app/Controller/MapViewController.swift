//  Copyright © 2017 cambri. All rights reserved.


import UIKit
import Firebase
import CoreLocation
import MapKit
import FacebookCore
import FacebookLogin


class MapViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {

    @IBOutlet weak var map: MKMapView!
    
    let manager = CLLocationManager()
    
    override func viewDidLoad() {

        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestWhenInUseAuthorization()
        manager.startUpdatingLocation()
        
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.map.delegate = self
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        printannotations()
    }
    @IBAction func signOutPressed(_ sender: Any) {
        if loginType == 1 {
        do {
            try Auth.auth().signOut()
        }
        catch {
            print("error logging out") //make into alert
            }
        }
        else{
        LoginManager().logOut()
        }
        
        navigationController?.popToRootViewController(animated: true)
        loginType = 0
    }
    
    @IBAction func addPinPressed(_ sender: Any) {
        self.performSegue(withIdentifier: "gotoAddPin", sender: self)
    }
    

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        let location = locations[0]

        let span:MKCoordinateSpan = MKCoordinateSpanMake(0.01, 0.01)
        let myLocation:CLLocationCoordinate2D = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude)
        let region:MKCoordinateRegion = MKCoordinateRegionMake(myLocation, span)
        map.setRegion(region, animated: true)

        self.map.showsUserLocation = true
    }


    func printannotations(){
        ref.child("Posts").observe(.childAdded, with: { (snapshot) in
            let pin = MyAnnotation(snapshot: snapshot)
            
            self.map.addAnnotation(pin)
           

    })

}
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation { return nil }

        var annotationView = map.dequeueReusableAnnotationView(withIdentifier: "Pin") as? MKPinAnnotationView
        if annotationView == nil {
            annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "Pin")
            annotationView?.canShowCallout = true
            annotationView?.rightCalloutAccessoryView = UIButton(type: .infoLight)
        } else {
            annotationView?.annotation = annotation
        }

        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        performSegue(withIdentifier: "calloutSegue", sender: view)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? CallOutController, let view = sender as? MKAnnotationView, let annotation = view.annotation as? MyAnnotation {
            vc.annotation = annotation 
            
        }
    }
}








