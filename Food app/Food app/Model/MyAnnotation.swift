import MapKit
import FirebaseDatabase

let ref = Database.database().reference()
class MyAnnotation: NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    

    let title: String?

    let time : String?
    let latitude : Double?
    let longitude : Double?
    let desc : String?
    var imageURL : String?
    
    init(snapshot: DataSnapshot) {
        self.title = (snapshot.value as AnyObject!)!["Title"] as! String!
        self.time = (snapshot.value as AnyObject!)!["Time"] as! String!
        self.latitude = (snapshot.value as AnyObject!)!["Latitude"] as! Double!
        self.longitude = (snapshot.value as AnyObject!)!["Longitude"] as! Double!
        self.desc = (snapshot.value as AnyObject!)!["Subtitle"] as! String!
        self.coordinate = CLLocationCoordinate2D(latitude: (Double(latitude!)), longitude: (Double(longitude!)))
        
        if let value = snapshot.value as? [String:Any] {
            if let url = value["Download URL"] as? String {
                self.imageURL = url
            }
        }
    }
}
