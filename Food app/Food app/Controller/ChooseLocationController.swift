//  Copyright © 2017 cambri. All rights reserved.

import Foundation
import MapKit
import CoreLocation
import Firebase

class ChooseLocationController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {
    
    @IBOutlet weak var map: MKMapView!
    let manager = CLLocationManager()
    
    var currentUserLocation: CLLocation?
    @IBOutlet weak var useUserLocation: UIButton!
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
     //   displayAnnotations()
        printannotations()
        
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestWhenInUseAuthorization()
        manager.startUpdatingLocation()//

        let uilgr = UILongPressGestureRecognizer(target: self, action: #selector(self.tapAction))
        map.addGestureRecognizer(uilgr)

    }
   
    
    @objc func tapAction(gestureRecognizer:UIGestureRecognizer){
        
        map.removeAnnotations(map.annotations)
        let touchPoint = gestureRecognizer.location(in: map)
        let newCoordinates = map.convert(touchPoint, toCoordinateFrom: map)
        
        let newPin = MKPointAnnotation()
        newPin.coordinate = newCoordinates
        
        map.addAnnotation(newPin)

        pickedLocationLat = newPin.coordinate.latitude
        pickedLocationLong = newPin.coordinate.longitude
    
       // displayAnnotations()
        printannotations()

        
    }
    
    
    @IBAction func saveLocation(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        
        guard locations.count > 0 else { return }
        
        currentUserLocation = locations.first
        
        let span:MKCoordinateSpan = MKCoordinateSpanMake(0.01, 0.01)
        let myLocation:CLLocationCoordinate2D = CLLocationCoordinate2DMake(currentUserLocation!.coordinate.latitude, currentUserLocation!.coordinate.longitude)
        let region:MKCoordinateRegion = MKCoordinateRegionMake(myLocation, span)
        map.setRegion(region, animated: true)
        
        self.map.showsUserLocation = true
        
        useUserLocation.isEnabled = true    }
    
    @IBAction func useCurrentLocation(_ sender: Any) {

        if let currentUserLocation = currentUserLocation {
            pickedLocationLat = currentUserLocation.coordinate.latitude
            pickedLocationLong = currentUserLocation.coordinate.longitude
        }
        
        navigationController?.popViewController(animated: true)
  
    }
    
    func printannotations(){
        ref.child("Posts").observe(.childAdded, with: { (snapshot) in
            let pin = MyAnnotation(snapshot: snapshot)
            
            self.map.addAnnotation(pin)
            
            
        })
        
    }
    
   /* func displayAnnotations() {
    
        let ref = Database.database().reference()
        
        ref.child("Posts").observe(.childAdded, with: { (snapshot) in
            
            let title = (snapshot.value as AnyObject!)!["Title"] as! String!
            let time = (snapshot.value as AnyObject!)!["Time"] as! String!
            let latitude = (snapshot.value as AnyObject!)!["Latitude"] as! Double!
            let longitude = (snapshot.value as AnyObject!)!["Longitude"] as! Double!
            let desc = (snapshot.value as AnyObject!)!["Description"] as! String!
            
            let annotation = MKPointAnnotation()
            
            annotation.coordinate = CLLocationCoordinate2D(latitude: (Double(latitude!)), longitude: (Double(longitude!)))
            annotation.title = title
            annotation.subtitle = desc
            annotation.subtitle = time
            self.map.addAnnotation(annotation)
            
        })}*/

}
