//  Copyright © 2017 cambri. All rights reserved.

import UIKit
import Firebase
import FirebaseAuth
import FacebookCore
import FacebookLogin

var loginType = 0

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

extension UIViewController {
    func showAlert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: {action in
        }))
        alertController.view.tintColor = UIColor.blue
        self.present(alertController, animated: true, completion: nil)
    }
}
class ViewController: UIViewController, LoginButtonDelegate {

    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var passwordTextfield: UITextField!
    @IBOutlet weak var image: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        image.image = UIImage(named: "logo")

        let FBLoginButton = LoginButton(readPermissions: [.publicProfile])
        let newCenter = CGPoint(x: self.view.frame.width / 2, y: self.view.frame.height - 70)
        FBLoginButton.center = newCenter
        view.addSubview(FBLoginButton)
        
        FBLoginButton.delegate = self as LoginButtonDelegate
        
        self.hideKeyboardWhenTappedAround()
        
    }
    
    func loginButtonDidLogOut(_ loginButton: LoginButton) {
    
    }
    
    func loginButtonDidCompleteLogin(_ loginButton: LoginButton, result: LoginResult) {
        loginType = 2
        switch result {
        case .success:
            let accessToken = AccessToken.current
            guard let accessTokenString = accessToken?.authenticationToken else { return}
            
            let credentials = FacebookAuthProvider.credential(withAccessToken: accessTokenString)
            Auth.auth().signIn(with: credentials, completion: {( user, error) in
                if error != nil {
                    print("something went wrong")
                    return
                }
                self.performSegue(withIdentifier: "goToMap", sender: self)
            })
        default:
            break
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func signInPressed(_ sender: Any) {
        loginType = 1
        Auth.auth().signIn(withEmail: emailTextfield.text!, password: passwordTextfield.text!) { (user, error) in
            if error != nil {
                print(error!)
                    
                    if let errorCode = AuthErrorCode(rawValue: error!._code) {
                        
                        switch errorCode {
                            
                        case .wrongPassword:
                            self.showAlert(title: "Info", message: "Wrong password")
                            
                        case .userNotFound:
                            self.showAlert(title: "Info", message: "The email you are trying to use have not been registred")
                            
                        case .networkError:
                            self.showAlert(title: "Info", message: "Network issues")
                        default:
                            self.showAlert(title: "Info", message: "Unknown error")
                        }
                    }
            }
                
             else {
                self.performSegue(withIdentifier: "goToMap", sender: self)
            }
        }
    }
}

