//  Copyright © 2017 cambri. All rights reserved.

import UIKit
import Firebase
import FirebaseDatabase
import CoreLocation
import FirebaseStorage

var pickedLocationLat: Double = 0.00
var pickedLocationLong: Double = 0.00


class AddPinController: UITableViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var titleTextfield: UITextField!
    
    @IBOutlet weak var descriptionTextfield: UITextField!
    @IBOutlet weak var longitudeTextfield: UITextField!
    @IBOutlet weak var latitudeTextfield: UITextField!
    @IBOutlet weak var imagePin: UIImageView!
    
    @IBOutlet weak var dateFild: UITextField!
    @IBOutlet weak var timeField: UITextField!
    

    let datePicker = UIDatePicker()
    let timePicker = UIDatePicker()
    var finalDate:String = ""
    
    var locManager = CLLocationManager()
    var currentLocation: CLLocation!
    var ref:DatabaseReference?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        createDatePicker()
        self.hideKeyboardWhenTappedAround()
        
    }
    
    
    @IBAction func changeImage(_ sender: Any) {
        
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        
        let actionSheet = UIAlertController(title: "Photo Source", message: "Choose a source", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action: UIAlertAction) in
            imagePickerController.sourceType = .camera
            self.present(imagePickerController, animated: true, completion: nil)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action: UIAlertAction) in
            imagePickerController.sourceType = .photoLibrary
            self.present(imagePickerController, animated: true, completion: nil)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
    
    }
    
        func createDatePicker() {
            let toolbar = UIToolbar()
            toolbar.sizeToFit()
    
            let done = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressed))
            toolbar.setItems([done], animated: false)
    
            dateFild.inputAccessoryView = toolbar
            dateFild.inputView = datePicker
            datePicker.datePickerMode = .dateAndTime
            let loc = Locale(identifier: "en_GB")
            self.datePicker.locale = loc
            
    }
    
        @objc func donePressed() {
            let formatter = DateFormatter()
            formatter.dateStyle = .medium
            formatter.timeStyle = .medium
            formatter.dateFormat =  "dd.MM.yy HH:mm"
            let dateString = formatter.string(from: datePicker.date)

            dateFild.text = "\(dateString)"
            
            self.view.endEditing(true)
            
        }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        imagePin.image = image
        
        picker.dismiss(animated: true, completion: nil)

}
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func chooseLocation(_ sender: Any) {
        self.performSegue(withIdentifier: "chooseLocation", sender: self)
    }
    

    func checkDate(){
        if dateFild.text == "" {
            
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "dd.MM.yy"
            let currentDate = formatter.string(from: date)
            finalDate = currentDate + " 23:59"
        }
        else {
            finalDate = dateFild.text!
        }
    }
    
    func post() {
        checkDate()

        if pickedLocationLat != 0.00, pickedLocationLong != 0.00 {
            if  titleTextfield.text != "", descriptionTextfield.text != "" {
                ref = Database.database().reference()
                if imagePin.image == nil {
                    let defaultValue = "empty"
                    self.ref?.child("Posts").childByAutoId().setValue(["Title": self.titleTextfield.text!, "Time": finalDate, "Subtitle": self.descriptionTextfield.text!, "Latitude": pickedLocationLat, "Longitude": pickedLocationLong, "Download URL": defaultValue])
                    
                        self.performSegue(withIdentifier: "goBackMap", sender: self)
                        pickedLocationLat = 0.00
                        pickedLocationLong = 0.00
                } else {
                    let storageRef = Storage.storage().reference().child("images/\(NSUUID().uuidString)/image.png")
                    if let uploadData = UIImagePNGRepresentation(self.imagePin.image!) {
                        storageRef.putData(uploadData, metadata: nil, completion: { (metadata, error) in
                            
                            if error != nil {
                                print("error")
                                return
                            } else {
                                let downloadURL = metadata?.downloadURL()?.absoluteString
                                
                                self.ref?.child("Posts").childByAutoId().setValue(["Title": self.titleTextfield.text!, "Time": self.finalDate, "Subtitle": self.descriptionTextfield.text!, "Latitude": pickedLocationLat, "Longitude": pickedLocationLong, "Download URL": (downloadURL)])
                                
                                self.performSegue(withIdentifier: "goBackMap", sender: self)
                                pickedLocationLat = 0.00
                                pickedLocationLong = 0.00                            }
                        }
                        )
                    }
                }
            }
                
                    else {
                        print("Please enter title and description")
                        showAlert(title:"Info", message: "Please enter both title and description")
                    }
            }
        
                
                else {
                    print("Please choose a location")
                    showAlert(title: "Location", message: "Please pick a location for your deal")//Craft alert
                }
            }
    


    
    @IBAction func saveButton(_ sender: Any) {
    post()

    }
    
}

